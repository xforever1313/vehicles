boolean debug = false;// turn to false to disable drawing vector lines
color backgroundColor;
ArrayList<Vehicle> vehicles;
ArrayList<Light> lights;
float size =  20; //scales vehicle
PShape light;

Boolean drawPicture = false;
Boolean lightsOut = false;

void setup() {
  size(800, 600, P2D);
  backgroundColor = color(175);
  background(backgroundColor);
  
  //make lights
  lights = new ArrayList<Light>();
  createLightShape();
 for (int j = 0; j < 10; j++) {
    lights.add(new Light(random(width), random(height), random(100, 500)));
  }

  //make vehicles
  vehicles = new ArrayList<Vehicle>();
  //PShape v = createVehicleShape();
  for (int i = 0; i < 15; i++) {
    vehicles.add(createNextVehicle());
  }
}

void createLightShape() {
  fill(color(255, 255, 120));
  stroke(color(255, 205, 100));
  light = createShape(ELLIPSE, 0, 0, 20, 20);
}

void updateVehicles(){
  for (int i = 0; i < vehicles.size(); ++i){
      vehicles.get(i).update();
      if (!isInCanvas(vehicles.get(i).position)){
          vehicles.set(i, null);
          vehicles.set(i, createNextVehicle());
          vehicles.get(i).update();
      }
      vehicles.get(i).display();
  }
}

void draw() {
  if (!drawPicture){
    background(backgroundColor);
  }
  for (Light l : lights) {
    l.display();
    l.drag();
  }
  updateVehicles();
}

void keyTyped(){
   if ((key == 'd') || (key == 'D')){
       drawPicture = !drawPicture;
   }
   else if ((key == 'l') || (key == 'L')){
       lightsOut = !lightsOut;
   } 
}

void mousePressed()
{
  for (Light l : lights)
    l.clicked(mouseX, mouseY);
}



void mouseReleased()
{
  for (Light l : lights)
    l.stopDragging();
}

