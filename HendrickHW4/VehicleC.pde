class VehicleC extends Vehicle{
    VehicleC(float x_, float y_, ArrayList<Light> lights_){
        super(x_, y_, lights_, color(0, 255, 0), 'C');
    } 
    
    void calculateLeftForce(){
        lForce = new PVector(1, 0);
        lForce.setMag(rSensorVal + lSensorVal);
        lForce.rotate(velocity.heading()); 
    }
    
    void calculateRightForce(){
        rForce = new PVector(1, 0);
        rForce.setMag(rSensorVal + lSensorVal);
        rForce.rotate(velocity.heading());
    }
}
