float DistanceSquared(PVector v1, PVector v2){
    float distance = ((v1.x - v2.x) * (v1.x - v2.x)) + ((v1.y - v2.y) * (v1.y - v2.y));
    return max(distance, 0.00000001);  //Do not want to devide by zero
}

PVector midpoint(PVector v1, PVector v2){
    return new PVector ((v1.x + v2.x)/2, (v1.y + v2.y)/2); 
}

Boolean isInCanvas(PVector v){
    return (v.x >= 0) && (v.x <= width) && (v.y >= 0) && (v.y <= height); 
}
