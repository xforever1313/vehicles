class Sensor{
  PVector sensorOffset;
  PVector position;
  float len;
  
  Sensor(float xOffset, float yOffset, float len){
      sensorOffset = new PVector(xOffset, yOffset);
      position = new PVector(0, 0);
      this.len = len;
  }
  
  void update(PVector centerPosition, PVector rotation){
      PVector rotateVector = sensorOffset.get();
      rotateVector.rotate(rotation.heading());
      position = PVector.sub(centerPosition, rotateVector);
  }
  
  void display(PVector rotation){
      pushMatrix();
      translate(position.x, position.y);
      rotate(rotation.heading());
      stroke(0);
      noFill();
      line(-size/4, 0, -len, 0);
      arc(0, 0, size/2, size/2, HALF_PI - 0.1, HALF_PI + PI + 0.1, OPEN);
      fill(0);
      popMatrix();
  }
  
}
