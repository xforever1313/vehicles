class VehicleB extends Vehicle{
    VehicleB(float x_, float y_, ArrayList<Light> lights_){
        super(x_, y_, lights_, color(0, 0, 255), 'B');
    } 
    
    void calculateLeftForce(){
        lForce = new PVector(1, 0);
        lForce.setMag(rSensorVal);
        lForce.rotate(velocity.heading()); 
    }
    
    void calculateRightForce(){
        rForce = new PVector(1, 0);
        rForce.setMag(lSensorVal);
        rForce.rotate(velocity.heading());
    }
}
