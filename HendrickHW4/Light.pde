class Light {

  PVector position;
  PShape s;
  float brightness;
  

  //for draggability
  boolean dragging = false; // Is the object being dragged?
  PVector dragOffset;  // holds the offset for when object is clicked on
  float scale;

  Light(float x_, float y_, float brightness_) {
    position = new PVector(x_, y_);
    brightness = brightness_;
    scale = brightness/100; 
    dragOffset = new PVector(0.0, 0.0);
  }

  void display() {
   
    pushMatrix();
    translate(position.x, position.y);
    scale(scale);
    if (!lightsOut){
      fill(color(255, 255, 120));
      stroke(color(255, 205, 100));
    }
    else{
      fill(color(95));
      stroke(color(0)); 
    }
    ellipse(0, 0, 20, 20);  
    popMatrix();
  }


  // The methods below are for mouse interaction
  void clicked(int mx, int my) {
    float d = dist(mx, my, position.x, position.y);
    if (d < 20 * scale *.5) {
      dragging = true;
      dragOffset.x = position.x-mx;
      dragOffset.y = position.y-my;
    }
  }

  void stopDragging() {
    dragging = false;
  }



  void drag() {
    if (dragging) {
      position.x = mouseX + dragOffset.x;
      position.y = mouseY + dragOffset.y;
    }
  }
}

