abstract class Vehicle {

  PVector position, velocity, acceleration, direction;
  Sensor leftSensor, rightSensor; 
  ArrayList<Light> lights;
  float  side, ahead; //for relative position of sensor
  float lSensorVal, rSensorVal; //value per sensor
  PVector leftSensorPos, rightSensorPos, lForce, rForce, lEndPoint, rEndPoint;
  //These values need to be tweaked for your world
  float minSpeed = 0.1;
  float maxSpeed = 2;
  float maxForce = 1;

  float frictionFactor = 0.3;
  PVector friction;

  color bodyColor;
  
  Character identity;

  Vehicle(float x_, float y_, ArrayList<Light> lights_, color bodyColor_, Character identity_) {
    side = size * 0.5; 
    ahead = size;
    lights = lights_;
    position = new PVector(x_, y_);
    velocity = new PVector(1, 0);
    friction = new PVector(0, 0);
    acceleration = new PVector(0, 0);
    direction = new PVector(1, 0);
    leftSensor = new Sensor(-ahead * 2, -size * 0.3, ahead);
    rightSensor = new Sensor(-ahead * 2, size * 0.3, ahead);
    bodyColor = bodyColor_;
    identity = identity_;
  }

  void update() {
    updateSensorPositions();
    getInputs();
    calcWheelForce();
    move();
  }

  void display() {
    pushMatrix();
    translate(position.x, position.y);
    rotate(velocity.heading());
    
    //chassis
    stroke(0);
    fill(bodyColor);
    quad(-ahead, -side, ahead, -side, ahead, side, -ahead, side);
      
    fill(125);
    //right wheel
    quad(-ahead/2, side, -ahead/2, 1.5 * side, -ahead, 1.5 * side, -ahead, side);

    //left wheel
    quad(-ahead/2, -side, -ahead/2, -1.5 * side, -ahead, -1.5 * side, -ahead, -side);

    fill(0);
    textAlign(CENTER, CENTER);
    text(identity, 0, 0);

    popMatrix();
    leftSensor.display(velocity);
    rightSensor.display(velocity);
  }

  void updateSensorPositions() {
     leftSensor.update(position, velocity);
     rightSensor.update(position, velocity);
  }

  void getInputs() {
    // calculate how much input we have from each sensor. 
    lSensorVal = 0;
    rSensorVal = 0;
    if (!lightsOut){
      for (Light l : lights) {
        lSensorVal += (l.brightness / DistanceSquared(leftSensor.position, l.position));
        rSensorVal += (l.brightness / DistanceSquared(rightSensor.position, l.position));      
      }
    }
  }

  abstract void calculateLeftForce();
  abstract void calculateRightForce(); 
  
  void calcWheelForce() {
    //calculate the force applied to left and right wheels
    //The direction of both forces will be in the direction of the vehicle
    //The magnitude of the force will be based on lSensorVal or rSensorVal
    //depending on the wiring
    //limit these by maxForce
    
    calculateLeftForce();
    calculateRightForce();

    lForce.limit(maxForce);
    rForce.limit(maxForce);
        
    lEndPoint = PVector.add(lForce, leftSensor.position);
    rEndPoint = PVector.add(rForce, rightSensor.position);
    
    direction = PVector.sub(rEndPoint, lEndPoint);
    direction = new PVector(-direction.y, direction.x); 
    
    if (debug) {
      stroke(0);
      line(lEndPoint.x, lEndPoint.y, leftSensor.position.x, leftSensor.position.y);
      line(rEndPoint.x, rEndPoint.y, rightSensor.position.x, rightSensor.position.y);
      
      stroke(0,255,0);
      line(lEndPoint.x, lEndPoint.y, rEndPoint.x, rEndPoint.y);
      stroke(255, 0, 0);
      PVector mid = midpoint(lEndPoint, rEndPoint);
      pushMatrix();
      translate(mid.x, mid.y);
      line(0, 0, direction.x, direction.y);
      popMatrix();
    }
  }
  
  PVector calculateVelocity(){
      direction.normalize();
      direction.mult((lSensorVal + rSensorVal) * 10);
      acceleration.add(direction);

      friction = velocity.get();
      friction.normalize();
      friction.mult(-frictionFactor * velocity.mag());
      acceleration.add(friction);
      return PVector.add(velocity, acceleration);
  }
  
  void move(){
      PVector newVelocity = calculateVelocity();
      
      if (newVelocity.mag() > maxSpeed){
          newVelocity.setMag(maxSpeed); 
      }
      //When the mag is zero, PVector's setMag and normalize doesn't do anything, so we need to 
      //create our own new vector
      else if (newVelocity.magSq() == 0){  
          newVelocity.x = minSpeed;
          newVelocity.rotate(velocity.heading());
      }
      else if (newVelocity.mag() < minSpeed){
          newVelocity.setMag(minSpeed);
      }
      velocity = newVelocity.get();
      position.add(velocity);
      acceleration.mult(0);
  }
}

