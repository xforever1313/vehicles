class VehicleD extends Vehicle{
    VehicleD(float x_, float y_, ArrayList<Light> lights_){
        super(x_, y_, lights_, color(0, 255, 125), 'D');
    } 
    
    void calculateLeftForce(){
        lForce = new PVector(1, 0);
        lForce.setMag(rSensorVal + lSensorVal);
        lForce.rotate(velocity.heading()); 
    }
    
    void calculateRightForce(){
        rForce = new PVector(1, 0);
        rForce.setMag(rSensorVal + lSensorVal);
        rForce.rotate(velocity.heading());
    }
    
    PVector calculateVelocity(){
      PVector newVelocity = velocity.get();
      newVelocity.setMag(maxSpeed);
      direction.normalize();
      direction.mult((lSensorVal + rSensorVal) * 10);
      acceleration.sub(direction);

      friction = velocity.get();
      friction.normalize();
      friction.mult(-frictionFactor * velocity.mag());
      acceleration.add(friction);
      
      acceleration.limit(newVelocity.mag() - minSpeed);
      return PVector.add(newVelocity, acceleration);
  }
}
