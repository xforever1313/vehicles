class VehicleA extends Vehicle{
    VehicleA(float x_, float y_, ArrayList<Light> lights_){
        super(x_, y_, lights_, color(255, 0, 0), 'A');
    } 
    
    void calculateLeftForce(){
        lForce = new PVector(1, 0);
        lForce.setMag(lSensorVal);
        lForce.rotate(velocity.heading()); 
    }
    
    void calculateRightForce(){
        rForce = new PVector(1, 0);
        rForce.setMag(rSensorVal);
        rForce.rotate(velocity.heading());
    }
}
