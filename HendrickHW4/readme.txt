Vehicles!
Seth Hendrick

Instructions:
    d - Turns on and off draw mode, the background does not reset when in draw mode.
    L - Turns on and off the lights.
    Left click - clicking and dragging a light will move it.

Car types:
    A - The left sensor is tied to the left wheel, and the right sensor is tied to the right wheel
    B - The left sensor is tied to the right wheel, and the left sensor is tied to the left wheel.
    C - Both sensors are tied to both wheels.
    D - Like C, where both sensors are tied to both wheels, but the light slows the motor down instead of speeding it up

Design notes:
    Since the thing being drawn are vehicles, there is a Vehicle class.  The vehicle class has the ability to update the status of the vehicle, and draw it.  A Vehicle class calculates the amount of light being shined onto each sensor, and then calculates the force each wheel exerts on the vehicle.  From there, it determines the acceleration based on the frictional force and the force from the wheels, and adds the acceleration to the velocity vector.  The velocity vector then changes the position vector.  After, the acceleration is reset to zero, as the acceleration shouldn't be consistently increasing.
  
    There are 4 types of vehicles that needed to be constructed: A, B, C, D. All four of them need a different way of calculating the left and right force, but (almost) everything else is similar.  Therefore, Vehicle is a base class that A, B, C, and D must extend and implement the calculateRightForce and calculateLeftForce methods depending on how they are wired up.  Since Vehicle D is special from the other vehicles, the way it calculates velocity is a bit different, so its CalculateVelocity overrides the Vehicle class's method.
    
    To create a vehicle, a vehicle factory file exists with the single method "createNextVehicle()".  This generates a random start position along the edge of the canvas for the vehicle to spawn, and then picks one of the four types of vehicles to spawn.
    
    The math file has a few useful math functions.  The DistanceSquared method is used when calculating the amount of energy on each sensor (brightness/distanceSquared for each light).  It will not return zero, as dividing by zero is bad.  It will instead return the calculated distance, or a really small value just greater than zero.  The math file also includes a midpoint method, which is used when drawing the debug lines, and an isInCanvas method, which is used to see if a vehicle is in the canvas or not.
    
