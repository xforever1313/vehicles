Vehicle createNextVehicle(){
   Vehicle ret;
   float minAngle, maxAngle;
   PVector startPosition;
   PVector startAngle = new PVector(1, 0);
   
   float position = random(0, 100);
   
   //Top
   if (position < 25){
        startPosition = new PVector(random(0, width), 0);
        startAngle.rotate(random(0, PI));
   }
   
   //Right
   else if (position < 50){
       startPosition = new PVector(width, random(0, height));
       startAngle.rotate(random(HALF_PI, HALF_PI + PI));     
   }
   
   //Bottom
   else if (position < 75){
       startPosition = new PVector(random(0, width), height);
       startAngle.rotate(random(PI, TWO_PI));
       
   }
   
   //Left
   else{
       startPosition = new PVector(0, random(0, height));
       startAngle.rotate(random(-HALF_PI, HALF_PI));  
   }
   
   float type = random(0, 100);
   if (type < 30){
       ret = new VehicleA(startPosition.x, startPosition.y, lights);
   } 
   else if (type < 60){
       ret = new VehicleB(startPosition.x, startPosition.y, lights); 
   }
   else if (type< 80){
       ret = new VehicleC(startPosition.x, startPosition.y, lights); 
   }
   else{
       ret = new VehicleD(startPosition.x, startPosition.y, lights); 
   }
   
   ret.velocity.rotate(startAngle.heading());
   return ret;
}
